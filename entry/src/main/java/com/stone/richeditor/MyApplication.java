package com.stone.richeditor;

import ohos.aafwk.ability.AbilityPackage;
import ohos.app.Context;

public class MyApplication extends AbilityPackage {

    private static Context context;
    @Override
    public void onInitialize() {
        super.onInitialize();
        context = this;
    }

    public static Context getAppContext(){
        return context;
    }
}
