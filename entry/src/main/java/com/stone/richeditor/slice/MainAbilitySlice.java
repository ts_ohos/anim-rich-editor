package com.stone.richeditor.slice;

import com.stone.richeditor.ResourceTable;
import com.stone.richeditor.customtext.RichTextEditor;
import com.stone.richeditor.data.EditData;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.app.Environment;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVStorage;
import ohos.utils.net.Uri;

import java.io.File;
import java.io.FileDescriptor;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    Image mOpenPic ,mOpenCamera , mOtherCtr;

    private static final int REQUEST_CODE_PICK_IMAGE = 1023;
    private static final int REQUEST_CODE_CAPTURE_CAMEIA = 1022;
    private RichTextEditor editor;
    static final HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, 0x0001, "选择图片测试");

    private static final File PHOTO_DIR = new File(
            Environment.DIRECTORY_PICTURES );//+ "/DCIM/Camera");
    private File mCurrentPhotoFile;// 照相机拍照得到的图片

    private final int mImageRequestCode = 1123;

    private PixelMap mPixelMap;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        //获取存储权限
        requestPermissionsFromUser(new String[]{"ohos.permission.READ_USER_STORAGE"},
                mImageRequestCode);
        editor = (RichTextEditor) findComponentById(ResourceTable.Id_richEditor);
        mOpenPic = (Image) findComponentById(ResourceTable.Id_openpic);
        mOpenPic.setClickedListener(this::openPicture);
        mOpenCamera = (Image) findComponentById(ResourceTable.Id_opencamera);
        mOpenCamera.setClickedListener(this::openCamera);
        mOtherCtr = (Image) findComponentById(ResourceTable.Id_other);
        mOtherCtr.setClickedListener(this::openOther);

    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if(requestCode == mImageRequestCode && null != resultData) {
            //选择的Img对应的Uri
            String chooseImgUri = resultData.getUriString();
            //定义图片来源对象
            ImageSource imageSource = null;
            //获取选择的Img对应的Id
            String chooseImgId=null;
            //这里需要判断是选择了文件还是图库
            if(chooseImgUri.lastIndexOf("%3A")!=-1){
                chooseImgId = chooseImgUri.substring(chooseImgUri.lastIndexOf("%3A")+3);
            }
            else {
                chooseImgId = chooseImgUri.substring(chooseImgUri.lastIndexOf('/')+1);
            }
            //获取图片对应的uri，由于获取到的前缀是content，我们替换成对应的dataability前缀
            Uri uri=Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI
                    ,chooseImgId);
            insertBitmap(uri.toString());
        }
    }
    /**
     * 添加图片到富文本剪辑器
     *
     * @param imagePath
     */
    private void insertBitmap(String imagePath) {
        editor.insertImage(imagePath);
    }
    public void openPicture(Component component){

        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder().
                withAction("android.intent.action.GET_CONTENT").build();
        intent.setOperation(operation);
        intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        intent.setType("image/*");
        intent.setType("image/jpeg");
        startAbilityForResult(intent, mImageRequestCode);
    }

    private void openCamera(Component component) {

    }

    private void openOther(Component component) {

        if(null != editor) {
            List<EditData> editList = editor.buildEditData();
            // 下面的代码可以上传、或者保存，请自行实现
            dealEditData(editList);
        }
    }
    /**
     * 负责处理编辑数据提交等事宜，请自行实现
     */
    protected void dealEditData(List<EditData> editList) {
        for (EditData itemData : editList) {
            if (itemData.inputStr != null) {
                // Log.d("RichEditor", "commit inputStr=" + itemData.inputStr);
            } else if (itemData.imagePath != null) {
                //Log.d("RichEditor", "commit imgePath=" + itemData.imagePath);
            }

        }
    }
}
