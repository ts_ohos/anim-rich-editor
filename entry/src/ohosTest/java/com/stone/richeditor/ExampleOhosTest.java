package com.stone.richeditor;

import com.stone.richeditor.customtext.RichTextEditor;
import com.stone.richeditor.data.DataImage;
import com.stone.richeditor.data.DeletableEditText;
import com.stone.richeditor.data.EditData;
import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.Size;
import org.junit.Test;
import org.junit.runner.manipulation.Ordering;

import static org.junit.Assert.*;

public class ExampleOhosTest {

    Context context = MyApplication.getAppContext();

    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.stone.richeditor", actualBundleName);
    }

    @Test
    public void testDatabaseInstance(){

        DataAbilityHelper helper=DataAbilityHelper.creator(context);
        assertNotNull(helper);
    }

    @Test
    public void testRichEditorInstance(){
        RichTextEditor richTextEditor = new RichTextEditor(context);
        assertNotNull(richTextEditor.buildEditData());
    }

    @Test
    public void testPixelTransform(){
        RichTextEditor richTextEditor = new RichTextEditor(context);
        int x = 20;
        assertTrue(x == richTextEditor.dip2px(10));

    }

    @Test
    public void testEditorInstance(){
        DeletableEditText editText =new DeletableEditText(context);
        assertNotNull(editText);
    }

    @Test
    public void testImageInstance(){
        DataImage image = new DataImage(context);
        assertNotNull(image);
    }

    @Test
    public void testPixelMap(){
        DataImage image = new DataImage(context);
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(10,10);
        PixelMap p = PixelMap.create(options);
        image.setPixelMaps(p);
        assertNotNull(image.getPixelMaps());
    }

}