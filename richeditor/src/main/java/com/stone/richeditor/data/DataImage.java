package com.stone.richeditor.data;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Image;
import ohos.app.Context;
import ohos.media.image.PixelMap;

/**
 * 这只是一个简单的ImageView，可以存放Bitmap和Path等信息
 * 
 * @author xmuSistone
 * 
 */
public class DataImage extends Image {
	
	private String absolutePath;

	private PixelMap pixelMap;

	public DataImage(Context context) {
		super(context);
	}

	public DataImage(Context context, AttrSet attrSet) {
		super(context, attrSet);
	}

	public DataImage(Context context, AttrSet attrSet, String styleName) {
		super(context, attrSet, styleName);
	}

	public String getAbsolutePath() {
		return absolutePath;
	}

	public void setAbsolutePath(String absolutePath) {
		this.absolutePath = absolutePath;
	}

	public PixelMap getPixelMaps() {
		return pixelMap;
	}

	public void setPixelMaps(PixelMap pixelMap) {
		this.pixelMap = pixelMap;
	}

}
