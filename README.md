# AnimRichEditor
 本项目是基于开源项目AnimRichEditor进行harmonyos化移植和开发的，可以通过项目标签以及[github地址](https://github.com/xmuSistone/AnimRichEditor)

 移植版本：源master
 
## 项目介绍
#### 项目名称：AnimRichEditor
#### 所属系列：harmonyos的第三方组件适配移植
#### 功能：AnimRichEditor：富文本编辑器，使用户能够插入/删除位图和文本到编辑视图与动画。
#### 项目移植状态：主要功能全部移植，harmonyos缺少接口部分 未移植
#### 调用差异：请参考demo使用，基本没有使用差异
#### 原项目地址：https://github.com/xmuSistone/AnimRichEditor
#### 编程语言：Java

## 安装教程
#### 方案一  
开发者在自己的项目中添加依赖  
```
  //核心引入 
  implementation project(':richeditor')
```
#### 方案二
项目根目录的build.gradle中的repositories添加：
```
    buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```

module目录的build.gradle中dependencies添加：
```
implementation 'com.gitee.ts_ohos:richeditor:1.0.0'
```

## 使用说明
- Step 1: 在引用项目'config.json'中申请文件读写权限 申请例子如下： 
```Xml
"reqPermissions": [
			{
				"name": "ohos.permission.WRITE_USER_STORAGE"
			},
			{
				"name": "ohos.permission.READ_USER_STORAGE"
			}
		]
```

- Step 2: 在'build.gradle'中添加依赖 "implementation project(":richeditor")"
- Step 3: 在项目的ability_main.xml配置如下:
```Java
  <ScrollView
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        xmlns:app="style"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:above="$id:title"
        ohos:orientation="vertical">
        <com.stone.richeditor.customtext.RichTextEditor
            ohos:id="$+id:richEditor"
            ohos:height="match_content"
            ohos:width="match_parent"/>
    </ScrollView>
```
- Step 4: 在项目 MainAbilitySlice 类中配置它,同时申请权限
```Java

   RichTextEditor editor;//这是组件最主要的操作对象
   public void initRichEditor() {
        //获取存储权限
        requestPermissionsFromUser(new String[]{"ohos.permission.READ_USER_STORAGE"},mImageRequestCode);
        editor = (RichTextEditor) findComponentById(ResourceTable.Id_richEditor); 
  }
```
- Step 5: 申请RichTextEditor对象以后就可以使用它了：RichTextEditor中主要方法如下：
```Java
insertImageExtens(PixelMap,String)其中第一个参数传入的是PixelMap对象, 也是editor需要插入的图片, 第二个是图片对应的地址;  
buildEditData()这是RichEditor文本返回的List对象, 里面包含了图片和文本信息，用于存储数据使用
```
## Demo效果演示
<img src="./picture/pic.png" width="400">
<img src="./picture/main.gif" width="400">

## 版本迭代
- v1.0.0项目初次提交


## 版本和许可信息

    Copyright 2015, xmuSistone
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

